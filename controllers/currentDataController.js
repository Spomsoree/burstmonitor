"use strict";

const
    config             = require('../config/config'),
    http               = require('http'),
    https              = require('https'),
    DeadlineController = require('./deadlineController'),
    FrontendController = require('./frontendController'),
    HelperController   = require('./helperController');

let firstCheck = true;

module.exports = {
    processBlock: (blocks) => {
        let block    = blocks['block'];
        let newStart = HelperController.blockStartToDate(blocks['newBlockWhen']);

        if (!HelperController._liveData[block]) {
            let lastBlock = Object.keys(HelperController._liveData).reverse()[0];
            let newBlock  = block;

            HelperController.logging({ fieldName: 'block', data: { block: block } });

            HelperController._liveData[block]       = {};
            HelperController._liveData[block].block = newBlock;

            DeadlineController.persistDeadline(lastBlock);

            Object.keys(HelperController._liveData).forEach(block => {
                if (block < newBlock - 20) {
                    DeadlineController.persistDeadline(block);
                    delete HelperController._liveData[block];
                }
            });

            module.exports.getBurstMarketData();
            module.exports.getRecentData();
            FrontendController.updateFrontend(true);
        }

        if (HelperController._liveData[block]) {
            checkField(block, { currentBlockStart: newStart }, 'currentBlockStart');
        }
    },

    processShares: (shares) => {
        let currentData  = null,
            historicData = null,
            deadline     = {};

        if (shares['shares'][0]) {
            deadline.poolDeadline = shares['shares'][0].deadline;
        }

        shares['shares'].forEach(userShare => {
            if (userShare['accountId'] === config.pool.user) {
                currentData = userShare;
            }
        });

        shares['historicShares'].forEach(historicUserShare => {
            if (historicUserShare['accountId'] === config.pool.user) {
                historicData = historicUserShare;
            }
        });

        if (currentData) {
            deadline.currentShare = currentData['share'] * 100;
            if (config.pool.type !== 'uray') {
                deadline.currentReward = currentData['estimatedReward'] / 100000000;
            } else {
                deadline.currentReward = currentData['estimatedReward'];
            }
            deadline.deadline = currentData.deadline;
        }

        if (historicData) {
            deadline.historicShare = historicData['share'] * 100;
            if (config.pool.type !== 'uray') {
                deadline.historicReward = historicData['estimatedReward'] / 100000000;
            } else {
                deadline.historicReward = historicData['estimatedReward'];
            }
            deadline.totalPayout = historicData['totalPayouts'] / 100000000;

            if (historicData['deferredPayouts']) {
                deadline.upcomingPayout = historicData['deferredPayouts'] / 100000000;
            }
            if (historicData['unconfirmedPayouts']) {
                deadline.upcomingPayout += historicData['unconfirmedPayouts'] / 100000000;
            }
            if (historicData['queuedPayouts']) {
                deadline.upcomingPayout += historicData['queuedPayouts'] / 100000000;
            }

            if (historicData['payoutFrequence']) {
                deadline.upcomingPayout = historicData['payoutFrequence'];
            }
        }

        let changed = checkFields(deadline, shares.block);

        if (changed) {
            FrontendController.updateFrontend();
        }
    },

    processAccount: (accounts) => {
        let user = {};

        accounts.forEach(account => {
            if (account['accountId'] === config.pool.user) {

                if (account['accountName']) {
                    user.username = account['accountName'];
                }
                if (account['estimatedCapacityTB']) {
                    user.capacity = account['estimatedCapacityTB'];
                }
                if (account['miningCapacityTB']) {
                    user.capacity = account['miningCapacityTB'];
                }

                HelperController._user = user;
            }
        });

    },

    getBurstMarketData: () => {
        https.get('https://api.coinmarketcap.com/v1/ticker/burst/?convert=EUR', function (res) {
            if (res.statusCode !== 200) {
                return;
            }

            let newData = '';
            res.on('data', function (response) {
                newData += response;
            }).on('end', function () {

                if (HelperController._liveData) {
                    let max      = Object.keys(HelperController._liveData).reverse()[0];
                    let euro     = JSON.parse(newData)[0]['price_eur'];
                    let change24 = JSON.parse(newData)[0]['percent_change_24h'];
                    let change7  = JSON.parse(newData)[0]['percent_change_7d'];

                    if (HelperController._liveData[max]) {

                        let changed = checkFields({
                            currentPrice: euro,
                            change_24:    change24,
                            change_7:     change7
                        }, max, 'market');

                        if (changed) {
                            FrontendController.updateFrontend();
                        }
                    }
                }
            });

        }).on('error', function () {
            module.exports.getBurstMarketData();
        });
    },

    getRecentData: () => {
        let uri = HelperController.getBaseUri();

        if (config.pool.type === 'pocc') {
            if (config.pool.https) {
                https.get(uri + '/wonblocks', function (res) {

                    prepareData(res);

                }).on('error', function () {
                    module.exports.getRecentData();
                });
            } else {
                http.get(uri + '/wonblocks', function (res) {

                    prepareData(res);

                }).on('error', function () {
                    module.exports.getRecentData();
                });
            }
        } else {
            let path = 'WebAPI';
            if (config.pool.type === 'ninja') {
                path = 'XHR'
            }
            if (config.pool.https) {
                https.get(uri + path + '/getRecentBlocks', function (res) {

                    prepareData(res);

                }).on('error', function () {
                    module.exports.getRecentData();
                });
            } else {
                http.get(uri + path + '/getRecentBlocks', function (res) {

                    prepareData(res);

                }).on('error', function () {
                    module.exports.getRecentData();
                });
            }
        }
    }
};

function checkFields (object, block, subObject = null) {
    let hasChanged = false;

    Object.keys(object).forEach(field => {
        let changed = checkField(block, object, field, subObject);

        if (changed) {
            hasChanged = true;
        }
    });

    return hasChanged;
}

function checkField (block, object, fieldName, subObject) {
    if (fieldName === 'currentBlockStart') {
        if (JSON.stringify(HelperController._liveData[block][fieldName]) !== JSON.stringify(object[fieldName])) {

            updateData(block, object, fieldName, subObject);
            return true;
        }
    } else if (object[fieldName] && (
        !HelperController._liveData[block][fieldName]
        || HelperController._liveData[block][fieldName]
        !== object[fieldName]
    )) {
        updateData(block, object, fieldName, subObject);
        return true;
    }
}

function updateData (block, object, fieldName, subObject) {
    let currentBlock = HelperController.getCurrentBlock();
    let hasDeadline  = object.lastActiveBlockHeight === currentBlock;
    let update       = false;

    if (!object.name) {
        HelperController.error('No User found.');
        return;
    }

    if (HelperController._liveData[block][fieldName]) {
        update = true;
    }

    if (hasDeadline) {
        if (currentBlock && currentBlock !== parseInt(block)) {
            HelperController.logging({ fieldName: fieldName, data: object }, update, block);
        } else {
            HelperController.logging({ fieldName: fieldName, data: object }, update);
        }
    }

    if (!hasDeadline && fieldName === 'deadline') {
        return;
    }

    if (subObject) {
        if (!HelperController._liveData[block][subObject]) {
            HelperController._liveData[block][subObject] = {};
        }

        HelperController._liveData[block][subObject][fieldName] = object[fieldName];
    } else {
        HelperController._liveData[block][fieldName] = object[fieldName];
    }
}

function prepareData (res) {
    if (res.statusCode !== 200) {
        return;
    }

    let newRecentData = '';
    res.on('data', function (response) {
        newRecentData += response;
    }).on('end', function () {

        if (config.pool.type === 'pocc') {
            let data          = newRecentData.replace(/(\r\n\t|\n|\r\t)/gm, '');
            let regex         = /tr id="(.*?)"(.*?td){2}>([0-9]+)/g;
            let match         = regex.exec(data);
            let recentData    = {};
            recentData.blocks = [];

            while (match != null) {
                match = regex.exec(data);

                if (match) {
                    recentData.blocks.push({
                        block:           match[1],
                        isOurBlock:      true,
                        ourBestDeadline: match[3]
                    });
                }
            }
            processData(recentData);
        } else {
            processData(JSON.parse(newRecentData));
        }
    });
}

function processData (inputData) {
    let hasChanged = false;
    let send       = false;

    inputData.blocks.forEach((block) => {

        if (!HelperController._liveData[block.block]) {
            firstCheckup(block);
            return;
        }

        let object = {
            won:          block['isOurBlock'],
            poolDeadline: block['ourBestDeadline']
        };

        let changed = checkFields(object, block.block);

        if (changed) {
            DeadlineController.persistDeadline(block.block);
            hasChanged = changed;
        }

        if (block['isOurBlock'] && !HelperController._liveData[block.block].send) {
            HelperController.sendPush(block.block, 'won');
            send = true;
        }

        if (block['ourBestAccountId'] === config.pool.user && !HelperController._liveData[block.block].send) {
            HelperController.sendPush(block.block, 'best');
            send = true;
        }

        if (send) {
            HelperController._liveData[block.block].send = true;
        }
    });

    if (hasChanged) {
        FrontendController.updateFrontend(true);
    }

    firstCheck = false;
}

function firstCheckup (block) {
    if (firstCheck) {
        block.won          = block['isOurBlock'];
        block.poolDeadline = block['ourBestDeadline'];
        DeadlineController.createOrUpdate(block.block, block);
    }
}