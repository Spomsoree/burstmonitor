"use strict";

const
    config             = require('../config/config'),
    DeadlineController = require('./deadlineController'),
    HelperController   = require('./helperController');

let io;

module.exports = {
    startFrontendSocket: (server) => {
        io = require('socket.io').listen(server);

        io.on('connection', (socket) => {

            socket.on('settings', (settings) => {
                HelperController.settings.show = settings;
                module.exports.updateFrontend(true);
            });
        });
    },

    updateFrontend: (newDataFromDatabase = null) => {
        let current = HelperController.getCurrentBlock();
        let output  = HelperController._liveData[current];
        let user    = HelperController._user;

        if (output) {

            output.block = parseInt(current);
            output.user  = user;
            output.pool  = config.pool.type;

            if (newDataFromDatabase) {

                DeadlineController.findDeadlines()
                    .then(blocks => {

                        if (blocks.length) {
                            output.blocks = blocks;
                        }

                        DeadlineController.getMinDeadline()
                            .then(minDeadline => {

                                output.minDeadline = minDeadline;

                                io.sockets.emit('data', output);
                            });
                    });
            } else {
                io.sockets.emit('data', output);
            }
        }
    }
};
