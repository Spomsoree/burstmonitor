"use strict";

const
    db               = require('../models'),
    HelperController = require('./helperController'),
    Sequelize        = require('sequelize');

let Op = Sequelize.Op;

module.exports = {
    persistDeadline: (block, exit = null) => {
        if (block) {
            if (HelperController._liveData[block] !== undefined) {
                module.exports.createOrUpdate(block, HelperController._liveData[block])
                    .then(() => {
                        if (exit) {
                            process.exit();
                        }
                    });
            } else {
                module.exports.createOrUpdate(block, {});
            }
        }
    },

    findDeadlines: () => {
        return new Promise((resolve, reject) => {
            db.Deadline.findAll({
                attributes: [
                    'block',
                    'currentReward',
                    'currentShare',
                    'deadline',
                    'historicReward',
                    'historicShare',
                    'poolDeadline',
                    'won'
                ],
                where:      getWhereObject()
            })
                .then(deadlines => {
                    if (!deadlines) {
                        resolve(false);
                    } else {
                        resolve(deadlines);
                    }
                })
                .catch(error => {
                    reject(error);
                });
        });
    },

    createOrUpdate: (block, data) => {
        return new Promise((resolve, reject) => {
            db.Deadline.findOrCreate({
                where:    {
                    block: block
                },
                defaults: data
            }).spread((deadline, created) => {
                if (!created) {
                    db.Deadline.update(data, {
                        where: {
                            block: block,
                        }
                    })
                        .then(updatedDeadline => {
                            resolve(updatedDeadline);
                        })
                        .catch(error => {
                            reject(error);
                        });
                }

                resolve(deadline);
            });
        });
    },

    getMinDeadline: () => {
        return new Promise((resolve, reject) => {
            db.Deadline.min('deadline', {
                where: getWhereObject()
            })
                .then(min => {
                    if (!min) {
                        resolve(false);
                    } else {
                        resolve(min);
                    }
                })
                .catch(error => {
                    reject(error);
                });
        });
    }
};

function getWhereObject () {
    let where        = {};
    let currentBlock = HelperController.getCurrentBlock();
    let settings     = HelperController.getSetting('show');

    if (settings.hasOwnProperty('byBlock')) {

        let byBlock = settings.byBlock;

        if (typeof byBlock === 'string') {
            byBlock = parseInt(byBlock);
        }

        if (byBlock !== 0) {
            where.block = { [Op.gt]: currentBlock - byBlock };
            return where;
        }
    } else if (settings.hasOwnProperty('byHours')) {

        let byHours = settings.byHours;

        if (typeof byHours === 'string') {
            byHours = parseInt(byHours);
        }

        if (byHours !== 0) {
            where.createdAt = { [Op.gt]: HelperController.getDateByHours(byHours) };
            return where;
        }
    }
}