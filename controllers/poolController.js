"use strict";

const
    config                = require('../config/config'),
    CurrentDataController = require('./currentDataController'),
    HelperController      = require('./helperController'),
    webSocket             = require('ws');

let io;

module.exports = {
    runWebSocket: (socket) => {
        io      = socket;
        let uri = HelperController.getBaseUri(true);

        switch (config.pool.type) {
            case 'uray':
                runUray(uri);
                break;

            case 'ninja':
                runNinja(uri);
                break;

            case 'pocc':
                runPocc(uri);
                break;
        }
    }
};

function runUray (uri) {

    let ws       = new webSocket(uri + '/webAPI/updates', 'updates', null);
    ws.onmessage = function (e) {
        if (e.data) {

            let block   = 'BLOCK:',
                share   = 'SHARES:',
                account = 'ACCOUNTS:';

            if (e.data.indexOf(block) === 0) {
                let data = JSON.parse(e.data.substr(block.length));
                CurrentDataController.processBlock(data);
            } else if (e.data.indexOf(share) === 0) {
                let data = JSON.parse(e.data.substr(share.length));
                CurrentDataController.processShares(data);
            } else if (e.data.indexOf(account) === 0) {
                let data = JSON.parse(e.data.substr(account.length));
                CurrentDataController.processAccount(data);
            }
        }
    };

    ws.onopen = function (e) {
        if (ws) {
            ws.send('Update!');
            ws.pingInterval = setInterval(function () {
                ws.send('ping');
            }, 30000);
        }
    };

    ws.onerror = function (e) {
        if (ws) {
            ws.close();
        }
    };

    ws.onclose = function (e) {
        if (ws && ws.pingInterval) {
            clearInterval(ws.pingInterval);
        }
        ws = null;
        setTimeout(function () {
            runUray(uri);
        }, 5000);
    };
}

function runNinja (uri) {
    let tempHistoric;
    let tempBlock;

    let ws       = new webSocket(uri + '/WS/updates', 'updates', null);
    ws.onmessage = function (e) {
        if (e.data) {
            let block    = 'BLOCK:',
                share    = 'SHARES:',
                account  = 'ACCOUNTS:',
                historic = 'HISTORIC:';

            if (e.data.indexOf(block) === 0) {
                let data = JSON.parse(e.data.substr(block.length));
                CurrentDataController.processBlock(data);
                tempBlock = data.block;
            } else if (e.data.indexOf(share) === 0) {
                let shares               = JSON.parse(e.data.substr(share.length));
                let modShares            = {};
                modShares.shares         = shares;
                modShares.historicShares = tempHistoric;
                modShares.block          = tempBlock;
                CurrentDataController.processShares(modShares);
            } else if (e.data.indexOf(account) === 0) {
                let accountData = JSON.parse(e.data.substr(account.length));
                CurrentDataController.processAccount(accountData);
            } else if (e.data.indexOf(historic) === 0) {
                tempHistoric = JSON.parse(e.data.substr(historic.length));
            }
        }
    };

    ws.onopen = function (e) {
        if (ws) {
            ws.send('Update!');
            ws.pingInterval = setInterval(function () {
                ws.send('ping');
            }, 30000);
        }
    };

    ws.onerror = function (e) {
        if (ws) {
            ws.close();
        }
    };

    ws.onclose = function (e) {
        if (ws && ws.pingInterval) {
            clearInterval(ws.pingInterval);
        }
        ws = null;
        setTimeout(function () {
            runNinja(uri);
        }, 5000);
    };
}

function runPocc (uri) {
    let poolDeadline;
    let block;

    let ws       = new webSocket(uri + '/ws', null, null);
    ws.onmessage = function (e) {
        if (e.data) {

            let data = JSON.parse(e.data);

            if (data.hasOwnProperty('height')) {
                block = data.height;
                CurrentDataController.processBlock({
                    block:        block,
                    newBlockWhen: Date.parse(data.created) / 1000
                });
                poolDeadline = data.deadline;
            } else if (data.hasOwnProperty('address')) {
                CurrentDataController.processShares({
                    block:          block,
                    shares:         [
                        {
                            deadline: poolDeadline
                        }, {
                            accountId: config.pool.user,
                            deadline:  data.deadline
                        }
                    ],
                    historicShares: [
                        {
                            accountId:       config.pool.user,
                            share:           data.historicalShare,
                            estimatedReward: data.pending,
                            payoutFrequence: data.payoutDetail,
                            totalPayouts:    data.pending
                        }
                    ]
                });

                CurrentDataController.processAccount([
                    {
                        accountId:           config.pool.user,
                        accountName:         data.name,
                        estimatedCapacityTB: data.effectiveCapacity
                    }
                ]);
            }
        }
    };

    ws.onopen = function (e) {
        if (ws) {
            ws.send(config.pool.user);
        }
    };

    ws.onerror = function (e) {
        if (ws) {
            ws.close();
        }
    };

    ws.onclose = function (e) {
        if (ws && ws.pingInterval) {
            clearInterval(ws.pingInterval);
        }
        ws = null;
        setTimeout(function () {
            runPocc(uri);
        }, 5000);
    };
}