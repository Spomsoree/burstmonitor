"use strict";

const
    config     = require('../config/config'),
    PushBullet = require('pushbullet');

Number.prototype.pad = function (size) {
    var s = String(this);
    while (s.length < (
        size || 2
    )) {s = '0' + s;}
    return s;
};

module.exports = {
    _liveData: {},
    _user:     {},
    settings:  {},

    getCurrentBlock: () => {
        let tempObjects = Object.keys(module.exports._liveData).reverse();

        if (tempObjects.length) {
            return parseInt(Object.keys(module.exports._liveData).reverse()[0]);
        }
    },

    getSetting: (setting) => {
        switch (setting) {
            case 'show':
                return module.exports.settings.show ? module.exports.settings.show : { byBlock: 500 };
        }
    },

    sendPush: (block, type) => {
        if (!config.push.pushbullet_api_key) {
            return;
        }

        let pusher = new PushBullet(config.push.pushbullet_api_key);
        let subject;
        let message;

        switch (type) {
            case 'won':
                if (!config.push.onWonPoolBlock) {
                    break;
                }
                let reward = 0;

                if (module.exports._liveData[block].historicReward) {
                    reward += module.exports._liveData[block].historicReward;
                }

                if (module.exports._liveData[block].currentReward) {
                    reward += module.exports._liveData[block].currentReward;
                }

                subject = 'Won: ' + reward.toFixed(8) + ' BURST';
                message = '';

                message += 'Block: ' + block;
                message += module.exports._liveData[block].historicReward ? "\nHistoric: "
                    + module.exports._liveData[block].historicReward : '';
                message += module.exports._liveData[block].currentReward ? "\nCurrent: "
                    + module.exports._liveData[block].currentReward : '';

                break;

            case 'best':
                if (!config.push.onBestDeadline) {
                    break;
                }
                subject = 'Best pool deadline';
                message = 'Deadline: ' + module.exports.deadlineToString(module.exports._liveData[block].deadline);

                break;
        }

        if (subject && message) {
            pusher.note(null, subject, message, () => {
            });
        }
    },

    error: (message) => {
        console.log('Error:', message);
    },

    logging: (object, update = null, block = null) => {
        let currentBlock = module.exports.getCurrentBlock();
        let fieldName    = object.fieldName;
        let value        = object.data[fieldName];
        let valueString  = '';
        let infoString   = '';

        if (config.log.data.indexOf(fieldName) >= 0) {

            if (fieldName === 'block') {
                currentBlock = value;
            }

            if (block) {
                infoString += block;
            } else if (config.log.data.indexOf('block') === -1) {
                infoString += currentBlock
            } else {
                for (let i = 0; i < currentBlock.toString().length; i++) {
                    infoString += '\xa0';
                }
            }

            if (update) {
                infoString += ' Updating';
            } else {
                if (fieldName === 'block') {
                    console.log(value + ' ■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■');
                }
                infoString += ' New     ';
            }

            if (currentBlock && fieldName !== 'block') {

                let maxLength;
                let logData = config.log.data;

                if (Array.isArray(logData)) {
                    logData = logData.toString();
                }

                maxLength = logData.split(/[\[,\]]/).reduce(function (a, b) {
                    return a.length > b.length ? a : b;
                }).length + 1;

                for (let i = fieldName.length; i < maxLength; i++) {
                    valueString += '\xa0';
                }

                /*
                poolDeadline
                deadline

                toLocaleLowerCase matches both
                 */
                if (fieldName.toLocaleLowerCase().includes('deadline')) {
                    let deadline = module.exports.deadlineToString(value);

                    if (fieldName.includes('deadline')) {
                        valueString += ' ' + (
                            value < 1000 ? '\x1b[32m' + deadline + '\x1b[0m' : deadline
                        );
                    } else {
                        valueString += ' ' + deadline;
                    }

                } else {
                    valueString += value < 0 ? value : ' ' + value;
                }

                console.log(infoString, fieldName, valueString);
            }
        }
    },

    getBaseUri: (ws = false) => {
        let https = config.pool.https;
        let uri   = https ? 'https' : 'http';

        if (ws) {
            uri = https ? 'wss' : 'ws';
        }

        let port = parseInt(config.pool.port) === 80 ? '' : ':' + config.pool.port;
        return uri + '://' + config.pool.host + port;
    },

    getDateByHours: (hours) => {
        let yesterday = new Date();
        return yesterday.setHours(yesterday.getHours() - hours);
    },

    blockStartToDate: (start) => {
        return new Date(start * 1000);
    },

    deadlineToString: (deadline) => {
        if (!deadline) {
            return false;
        }

        let day = (
            Math.trunc(deadline / 86400)
        );
        let hou = Math.trunc((
            deadline % 86400
        ) / 3600);
        let min = Math.trunc((
            deadline % 3600
        ) / 60);
        let sec = Math.trunc((
            deadline % 3600
        ) % 60);

        return (
            (
                day ? day + 'd ' : ''
            ) +
            (
                (
                    day || hou
                ) ? hou.pad() + ':' : ''
            ) +
            (
                (
                    day || hou || min
                ) ? min.pad() + ':' : '00:'
            ) +
            (
                (
                    day || hou || min || sec
                ) ? sec.pad() : ''
            )
        );
    }
};