const
    config             = require('./config/config'),
    db                 = require('./models'),
    DeadlineController = require('./controllers/deadlineController'),
    express            = require('express'),
    favicon            = require('serve-favicon'),
    FrontendController = require('./controllers/frontendController'),
    HelperController   = require('./controllers/helperController'),
    PoolController     = require('./controllers/poolController'),
    swig               = require('swig');

let app = express();

function startApp () {
    let server = app.listen(config.port);
    FrontendController.startFrontendSocket(server);
    PoolController.runWebSocket();
}

function exitApp () {
    let block = HelperController.getCurrentBlock();
    DeadlineController.persistDeadline(block, true);
}

db.sequelize.sync()
    .then(startApp)
    .catch(function (e) {
        throw new Error(e);
    });

process.stdin.resume();
process.on('exit', exitApp);
process.on('SIGINT', exitApp);
process.on('SIGUSR1', exitApp);
process.on('SIGUSR2', exitApp);
process.on('uncaughtException', exitApp);

app.use(express.static(__dirname + '/public'));
app.use(favicon(__dirname + '/public/images/favicon.ico'));

app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    next();
});

app.get('/', function (req, res) {
    res.send(
        swig.renderFile('./views/dashboard.html', { notLoaded: '-' })
    );
});
