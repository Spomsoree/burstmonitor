#!/bin/sh

git checkout .

git pull
pm2 stop monitor.js

npm install &
wait

if [ "$1" = "watch" ]
    then pm2 start monitor.js --watch
    else pm2 start monitor.js
fi

pm2 logs