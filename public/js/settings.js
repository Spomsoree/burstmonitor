let setting  = $('.setting');
let settings = $('#settings');

$('#save_settings').click(function () {
    let byBlock = $('.modal-item > div > .show-by-block').text();
    let byHours = $('.modal-item > div > .show-by-hours').text();

    if (byBlock) {
        updateSettings({ byBlock: parseInt(byBlock) });
        document.cookie = "settings=" + JSON.stringify({ byBlock: byBlock });
    } else if (byHours) {
        updateSettings({ byHours: parseInt(byHours) });
        document.cookie = "settings=" + JSON.stringify({ byHours: byHours });
    }
});

setting.focus(function () {
    selectElementContents(this);
});

setting.keypress(function (evt) {
    setting.not(this).html('');
    if (evt.which === 13) {
        evt.preventDefault();
    }
});

let rawSettings = document.cookie.split('=')[1];

if (rawSettings) {
    updateSettings(JSON.parse(rawSettings));
} else {
    updateSettings({ byBlock: 500 })
}

$(document).ready(function () {
    $('[data-toggle="tooltip"]').tooltip();
});

function selectElementContents (el) {
    let range = document.createRange();
    range.selectNodeContents(el);
    let sel = window.getSelection();
    sel.removeAllRanges();
    sel.addRange(range);
}