let
    block,
    currentBlockStart,
    myPoolShareGraph,
    poolDeadline,
    socket         = io(),
    bestPools      = [],
    currentBlocks  = [],
    myPoolShare    = [],
    poolWon        = [],
    won            = [],
    showGraphLines = {
        won:     true,
        best:    true,
        poolWon: false,
    };

let elements = {
    blockId:             {
        field: 'block'
    },
    historicReward:      {
        field: 'historicReward'
    },
    username:            {
        field: 'username',
        subOf: 'user'
    },
    currentReward:       {
        field: 'currentReward',
        func:  'cut'
    },
    historicShare:       {
        field: 'historicShare',
        func:  'cut'
    },
    currentShare:        {
        field: 'currentShare',
        func:  'cut'
    },
    currentPoolDeadline: {
        field: 'poolDeadline',
        func:  'deadline'
    },
    currentDeadline:     {
        field: 'deadline',
        func:  'deadline'
    },
    minHistoryDeadline:  {
        field: 'minDeadline',
        func:  'deadline'
    },
    totalPayoutValue:    {
        field: 'totalPayout',
        func:  'earning'
    },
    upcomingPayoutValue: {
        field: 'upcomingPayout',
        func:  'earning'
    },
    marketValue:         {
        field: 'currentPrice',
        subOf: 'market',
        func:  'cut'
    },
    totalPayout:         {
        field: 'totalPayout',
        func:  'cut',
        opt1:  3
    },
    upcomingPayout:      {
        field: 'upcomingPayout',
        func:  'cut',
        opt1:  3
    },
    capacity:            {
        field: 'capacity',
        subOf: 'user',
        func:  'cut',
        opt1:  2
    },
    marketChange_24:     {
        field: 'change_24',
        subOf: 'market',
        func:  'change',
        opt1:  '_24'
    },
    marketChange_7:      {
        field: 'change_7',
        subOf: 'market',
        func:  'change',
        opt1:  '_7'
    }
};

Number.prototype.pad = function (size) {
    var s = String(this);
    while (s.length < (
        size || 2
    )) {s = '0' + s;}
    return s;
};

socket.on('data', data => {
    if (data.pool) {
        poolSettings(data.pool);
    }
    updateInfoBoxes(data);
    updateData(data);
});

function poolSettings (pool) {
    if (pool === 'pocc') {
        elements.currentShare.field   = 'historicShare';
        elements.historicShare.field  = '';
        elements.currentReward.field  = 'historicReward';
        elements.historicReward.field = '';
    }
}

function updateSettings (object) {
    socket.emit('settings', object);

    if (object.hasOwnProperty('byBlock')) {
        $('.show-by-block').html(object.byBlock);
    } else if (object.hasOwnProperty('byHours')) {
        $('.show-by-hours').html(object.byHours);
        $('.show-by-hours-h').html(object.byHours + 'h');
    }
}

async function updateInfoBoxes (data) {
    Object.keys(elements).forEach(key => {
        let element    = elements[key];
        let updateData = data[element.field];

        if (data[element.subOf]) {
            updateData = data[element.subOf][element.field];
        }

        switch (element.func) {
            case 'cut':
                updateElement(key, cut(updateData, element.opt1));
                break;
            case 'deadline':
                updateElement(key, deadlineToString(updateData));
                break;
            case 'change':
                updateElement(key, prepareChange(updateData, element.opt1));
                break;
            case 'earning':
                data['market'] ? updateElement(key, estimateEarning(updateData, data['market'].currentPrice)) : null;
                break;
            default:
                updateElement(key, updateData);
        }

        if (key === 'upcomingPayout') {
            if (typeof data[key] === 'string') {
                document.getElementById('poccPayout').innerHTML = '<span class="stringPayout">' + data[key] + '</span>';
            }
        }
    });

    if (data.user) {
        $('.user-info').collapse('show');
    }

    if (data.upcomingPayout || data.totalPayout) {
        $('.payout').collapse('show');
    }

    if (data['market']) {
        $('.market-value').collapse('show');
    }

    if (data.poolDeadline && data.poolDeadline === data.deadline) {
        $('.alert-best-pool-dl').collapse('show');
    } else {
        $('.alert-best-pool-dl').collapse('hide');
    }
}

function updateData (data) {
    if (!poolDeadline || poolDeadline !== data.poolDeadline) {
        poolDeadline = data.poolDeadline;
    }
    if (!currentBlockStart || currentBlockStart !== data.currentBlockStart) {
        currentBlockStart = new Date(data.currentBlockStart);
    }

    if (data.blocks) {

        myPoolShare = [];
        poolWon     = [];
        won         = [];
        bestPools   = [];

        let blocks = data.blocks;
        blocks.forEach(block => {

            currentBlocks[block['block']] = block;

            // Shares
            if (block.historicShare) {
                myPoolShare.push([block['block'], parseFloat(block.historicShare)]);
            } else {
                myPoolShare.push([block['block'], null]);
            }

            // Won blocks
            if (block.won) {
                poolWon.push(block.block);
            }

            // Best deadlines
            if (block.deadline && block.deadline === block.poolDeadline) {
                bestPools.push(block.block);

                if (block.won) {
                    won.push(block.block);
                }
            }
        });
    }

    updateGraph();
}

function updateGraph () {
    if (!myPoolShare.length) {
        return;
    }

    if (myPoolShareGraph) {

        myPoolShareGraph.updateOptions({
            file: myPoolShare
        });

    } else {
        myPoolShareGraph = new Dygraph(
            document.getElementById('myPoolShare'),
            myPoolShare,
            {
                axisLineColor:      'rgba(67, 72, 87, 1.0)',
                gridLineColor:      'rgba(67, 72, 87, 0)',
                color:              'rgba(18, 171, 228, 1.0)',
                fillGraph:          true,
                digitsAfterDecimal: 3,
                axisLabelFontSize:  12,
                includeZero:        true,
                axes:               {
                    y: {
                        axisLabelFormatter: function (data) {
                            return parseFloat(data).toFixed(3) + ' %';
                        }
                    }
                },
                legendFormatter:    function (data) {
                    if (data.x == null) {
                        return '';
                    }

                    let string = data['xHTML'] + data['series'].map(
                        v => v['yHTML'] ? ': ' + v['yHTML'] + '%' : ''
                    );

                    if (currentBlocks[data.x] && currentBlocks[data.x].deadline) {
                        string += '<br/>' + deadlineToString(currentBlocks[data.x].deadline);
                    }

                    return string;
                },
                underlayCallback:   function (canvas, area, g) {
                    if (showGraphLines.poolWon) {
                        canvas.fillStyle = 'rgba(255, 255, 255, 1.0)';

                        poolWon.forEach(id => {
                            setFilling(id, canvas, area, g)
                        });
                    }

                    if (showGraphLines.best) {
                        canvas.fillStyle = 'rgba(124, 252, 0, 1.0)';

                        bestPools.forEach(id => {
                            setFilling(id, canvas, area, g)
                        });
                    }

                    if (showGraphLines.won) {
                        canvas.fillStyle = 'rgba(255, 0, 0, 1.0)';

                        won.forEach(id => {
                            setFilling(id, canvas, area, g)
                        });
                    }
                }
            }
        );
    }
}

function updateElement (id, data) {
    if (document.getElementById(id)) {
        document.getElementById(id).innerHTML = data ? data : '-';
    }
}

function estimateEarning (earning, price) {
    if (!price || !earning) {
        return;
    }

    return (
        earning * price
    ).toFixed(2);
}

function cut (value, to) {
    if (value) {
        value = parseFloat(value);

        if (to) {
            value = value.toFixed(to);
        } else {
            value = value.toFixed(7);
        }

    }

    return value;
}

function prepareChange (change, id) {
    if (!change) {
        return '-';
    }

    if (change > 0) {
        document.getElementById('marketChange' + id).classList.add('positive');
        document.getElementById('marketChange' + id).classList.remove('negative');
        return '+' + change;
    } else {
        document.getElementById('marketChange' + id).classList.add('negative');
        document.getElementById('marketChange' + id).classList.remove('positive');
        return change;
    }
}

function deadlineToString (deadline) {
    if (!deadline) {
        return false;
    }

    let day = (
        Math.trunc(deadline / 86400)
    );
    let hou = Math.trunc((
        deadline % 86400
    ) / 3600);
    let min = Math.trunc((
        deadline % 3600
    ) / 60);
    let sec = Math.trunc((
        deadline % 3600
    ) % 60);

    return (
        (
            day ? day + 'd ' : ''
        ) +
        (
            (
                day || hou
            ) ? hou.pad() + ':' : ''
        ) +
        (
            (
                day || hou || min
            ) ? min.pad() + ':' : '00:'
        ) +
        (
            (
                day || hou || min || sec
            ) ? sec.pad() : ''
        )
    );
}

function setFilling (blockId, canvas, area, g) {
    let left  = blockId - 0.5;
    left      = g.toDomCoords(left)[0];
    let right = blockId + 0.5;
    right     = g.toDomCoords(right)[0] - left;
    return canvas.fillRect(left, area.y, right, area.h);
}

function renderProgress () {
    let progress = calcProgress();

    document.getElementById('currentProgress').style.width = progress[1];
    updateElement('blockDuration', deadlineToString(progress[0]));
}

function calcProgress () {
    let now          = new Date();
    let secondsSoFar = Math.round((
        now - currentBlockStart
    ) / 1000);

    if (!poolDeadline) {
        return [secondsSoFar, 0];
    }

    if (secondsSoFar > poolDeadline) {
        secondsSoFar = poolDeadline;
    }

    let percent = parseFloat(100 * secondsSoFar / poolDeadline).toFixed(2);

    return [secondsSoFar, percent + '%'];
}

let showWon     = document.getElementById('toggleShowWon');
let showBest    = document.getElementById('toggleShowBest');
let showPoolWon = document.getElementById('toggleShowPoolWon');

showWon.addEventListener('click', () => {
    showWon.classList.toggle('active');
    showGraphLines.won = !showGraphLines.won;
    updateGraph();
});
showBest.addEventListener('click', () => {
    showBest.classList.toggle('active');
    showGraphLines.best = !showGraphLines.best;
    updateGraph();
});
showPoolWon.addEventListener('click', () => {
    showPoolWon.classList.toggle('active');
    showGraphLines.poolWon = !showGraphLines.poolWon;
    updateGraph();
});

$(function () {
    $('[data-toggle="tooltip"]').tooltip()
});

window.setInterval(renderProgress, 1000);

