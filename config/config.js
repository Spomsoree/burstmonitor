const
    fs = require('fs');

let config = {
    port: 1337,
    db:   {
        database: 'burstmonitor',
        username: 'root',
        password: 'root',
        options:  {
            host:    '127.0.0.1',
            port:    '3306',
            dialect: 'mysql',
            pool:    {
                max:  100,
                min:  0,
                idle: 10000
            },
            logging: false
        }
    },
    pool: {
        user:  '8618096741066992218',
        host:  'pool.burstcoin.space',
        type:  'ninja',
        port:  8124,
        https: false,
    },
    push: {
        onBestDeadline:     true,
        onWonPoolBlock:     true,
        pushbullet_api_key: null,
    },
    log:  {
        data: [
            'block',
            'deadline',
            'poolDeadline',
            'won',
            /*
            'historicShare',
            'historicReward',
            'currentShare',
            'currentReward',
            'upcomingPayout',
            'totalPayout',
            'currentPrice',
            'change_24',
            'change_7',
            'currentBlockStart'
            */
        ]
    }
};

function getConfiguration () {
    let modifiedConfig = config;
    let regexp         = /(.*)=(.*)/g;
    let result;
    let credentials;
    try {
        credentials = fs.readFileSync('credentials', 'utf8');
    } catch (err) {
        if (err.code === 'ENOENT') {
            console.log("Define your credentials with a credential file \n$ sudo nano credentials");
        }
    }

    while (result = regexp.exec(credentials)) {
        let key   = result[1];
        let value = result[2];
        let keys  = key.split('.');

        if (keys.length > 2) {
            modifiedConfig[keys[0]][keys[1]][keys[2]] = value;
        } else if (keys.length > 1) {
            modifiedConfig[keys[0]][keys[1]] = value;
        } else {
            modifiedConfig[keys[0]] = value;
        }

    }

    return modifiedConfig;
}

module.exports = getConfiguration();
