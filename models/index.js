const
    config    = require('../config/config'),
    db        = {},
    Sequelize = require('sequelize');

let sequelize = new Sequelize(config.db.database, config.db.username, config.db.password, config.db.options);

db.Deadline = sequelize['import']('./deadline.js');

db.sequelize = sequelize;
db.Sequelize = Sequelize;

module.exports = db;
