module.exports = function (sequelize, DataTypes) {
    return sequelize.define('deadline', {
        block:          {
            type:       DataTypes.INTEGER,
            primaryKey: true
        },
        deadline:       DataTypes.INTEGER,
        poolDeadline:   DataTypes.INTEGER,
        historicShare:  DataTypes.DECIMAL(11, 8),
        historicReward: DataTypes.DECIMAL(15, 8),
        currentShare:   DataTypes.DECIMAL(11, 8),
        currentReward:  DataTypes.DECIMAL(15, 8),
        won:            DataTypes.BOOLEAN
    });
};
