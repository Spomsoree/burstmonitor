# BURSTmonitor

## Requirements
 
This currently only works with pocc, uray and ninja pools

#### Linux
```
$ sudo apg-get install git
$ sudo apt-get install nodejs 
$ sudo apt-get install mysql-server 
$ mysql -u root -p
> CREATE DATABASE burstmonitor;
```

#### Mac
```
$ brew install git
$ brew install node
```

 
## Installation

```
$ git clone https://bitbucket.org/Spomsoree/burstmonitor.git
$ cd burstmonitor
$ npm install
```

## Updating

Navigate to the burstmonitor directory

```
$ ./update.sh
```

 
## Configuration

Create a file named ```credentials``` without file extension

```
$ sudo nano credentials
```

The file could look like this

```
db.password=mySQLpassword
push.pushbullet_api_key=x.xxXxXxxXxxXXXxXXXXXxxX
log.data=[block,deadline,poolDeadline]
```

Possible fields to log can be found in ``config/config.js``

#### General
* ``port`` defines on which port the webserver runs | default ``1337``
* ``db.database`` the name of the database
* ``db.user`` username to login to the database
* ``db.password`` password to login to the database

#### Burst
* ``pool.user`` the numeric burst id
* ``pool.type`` thy pool type ``pocc``|``uray``|``ninja``
* ``pool.host`` the url of you pool
* ``pool.https`` if your pool uses https

#### Logging
* ``log.data`` defines which data is logged

#### Push Notification
* ``push.onBestDeadline`` get a notification if you got the best pool deadline | default ``true``
* ``push.onWonPoolBlock`` get a notification if the pool won a block | default ``true``
* ``push.pushbullet_api_key`` your api key if you want to get notified on won blocks

## Usage

```
$ pm2 start monitor.js
$ pm2 logs
```

Then go to ``http://127.0.0.1:1337`` (or which port you have chosen)

## Gallery
![Dashboard](https://i.imgur.com/vm0zgG0.png | width=70)
![Dashboard](https://i.imgur.com/b3NsWJS.png | width=70)